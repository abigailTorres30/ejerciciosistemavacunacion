package Modelo;

import ufps.util.colecciones_seed.ColaP;

public class Municipio {

    private int id_municipio;

    private String nombre;

    private ColaP<Persona> personas = new ColaP();

    public Municipio() {
    }

    public Municipio(int id_municipio, String nombre) {
        this.id_municipio = id_municipio;
        this.nombre = nombre;
    }

    public int getId_municipio() {
        return id_municipio;
    }

    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ColaP<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ColaP<Persona> personas) {
        this.personas = personas;
    }

    @Override
    public String toString() {
        return "\n Municipio{" + "id_municipio=" + id_municipio + ", nombre=" + nombre + this.getListadoPersonas() + '}';
    }

    public void registrarPersona(Persona nueva) {
        this.personas.enColar(nueva, nueva.getPrioridad());
    }

    /**
     * Esté método obtiene las personas de un municipio WARNING!!!!!!!! LA COLA
     * SE BORRA (DEBE MODIFICAR EL MÉTODO)
     *
     * @return
     */
    public String getListadoPersonas() {
        String msg = "";

        while (!this.personas.esVacia()) {
            msg += "|\t" + this.personas.deColar().toString() + "\t|\n";
        }

        return msg;
    }

}
